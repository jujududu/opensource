---
header: 'Cours des licenses'
footer: 'Julien Dauliac -- ynov.casualty925@passfwd.com'
---

<!-- headingDivider: 3 -->
<!-- paginate: true -->
<!-- colorPreset: sunset -->


# Cours des licences

[TOC]

## Biens immatériels

[allez 👮](https://www.youtube.com/watch?v=sODZLSHJm6Q&pp=ygUZbGUgcGlyYXRhZ2UgYydlc3QgZHUgdm9sIA%3D%3D)

---

- Intangibles, que l’on ne peut pas toucher.
- Pas de concurrence sur les produits.

## Vol

> Le vol est la **soustraction** frauduleuse de la chose d'autrui.
Code pénal **[Article 311-1](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000006418127)**
> 

> [Retirer](https://fr.wiktionary.org/wiki/retirer), [dérober](https://fr.wiktionary.org/wiki/d%C3%A9rober)

Wikitionnaire
> 

## On ne peut pas voler de logiciel
- Le logiciel est un bien immatériel

## Internet

- Permet un coût technique marginal:
  e.g:
    - prolifération des ecommerces, elearning, etrucs.

# Licenses et modèles économiques

## Ayant droit

> ***L’auteur d’une œuvre de l’esprit jouit sur cette œuvre, du seul fait de sa création, d’un droit de propriété incorporelle exclusif et opposable à tous.*
Le Code de la Propriété Intellectuelle ([article L111-1](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006278868&cidTexte=LEGITEXT000006069414))**

---

- L’ayant droit choisi la licenses

---

- En entreprise, on cède ses droits à l’entreprise *(sauf pour les stagiaires)*

## Licenses privatives

- Classiques, propriétaires, fermées.

## Licenses partagiciels

- winrar
    - marketing viral
    - N’existe plus
- Difficulté à l’époque de toucher son publique
    - `1 million qui l’utilise et 100 qui payent == 1000 qui l’utilisent et 100 qui payent`

## Gratuiciel

- Monétisation indirecte:
    - Beaucoup utilisé pour les services: SAAS
    - Si le service est gratuit, c’est vous la marchandise

Examples:
- Google drive
- Discord

---

- Discord ne revend pas les données utilisateurs.
- Discord dilue ses parts.

## Freemium
- gratuites au début et vous incitent à payer.

---

Examples:
- Pay to win 
- Tinder

## Libre

- Gratuit
- Peuvent bénéficier du support de la communauté
- Internet permet un coût économique de partage marginal

### Copyleft

- Contagieux
- Un moyen de rendre un logiciel (ou autre) libre

### Résumé

![width:200px](assets/Untitled.png)

### Économiquement

- Efficacité redoutable:
    - Effet de réseau:
        - viral
        - populaires
    - Dévaste la concurrence
        - Chromium
        - KDE


# Droit francais

## Asymétrie
- Primauté du droit francais

---

- Les Brevets sont illégaux en france
- Les brevets logiciels sont légaux en Europe

---
## OEB
- Un organisme batard
- Pas de compte à rendre
- Pas de contrôle démocratique
- Pas de contrôle juridique

---
- Il faudrait changer la constitution de l'UE pour changer l’OEB

---
- Anti logiciel libre

---
- 3/4 des brevets logiciels accordés par l’OEB sont détenus par des pays extra-européens

---
- Dés

## Pattent troll
- des entreprises qui ne servent qu'a faire des procès.

---
### Des brevets inutiles
- brevets innutiles:
  - Double click: Microsoft, 2007
  - Achat en un click: Amazon, 1999
  - Brevet sur les liens hypertextes: IBM, 1998
  - Brevet sur les fenêtres: Xerox, 1984

---

## Primauté du droit américain
- Rentrer dans le système de brevet, c'est se soumettre au droit états-unien.

## Entrave à l'innovation
- Anti innovation
- Permet à des fims internationales de s’approprier des technologies
- Met en porte à faux de des petites et moyennes entreprise face à des géants

---
- L'innovation ne vient pas des grandes entreprise.

---

- Les grosse corp rachètent, financent, intègrent

# Merdification

## Cas Hashicorp

Tout les logiciels hashicorps sont passés de licences libre à BUSL.

## Monétisation du capital social et symbolique

Bourdieu:

- capital économique
- capital culturel
- capital symbolique
- capital social

Détérioration de bien publique ?

# Cyber war

- Une guerre de gagnée:
    
---
Logiciel libre hégémonique sur tout les plans:
  - Rentabilité
  - Droits Libertés individuelles
  - Performance

---

Mais

---
- Une nouvelle guerre suis son cours cours...

---
- Un combat sur les clouds providers et plus généralement les plateformes

---
- Le saas, nouvel outil de privation de libertés.

---
  Le droit d’auteur montre ses limites sur les bases de données à cause grace, des intelligences artificielles:
        - Libéré les modèles
        - Risques de la privation
        - Risque du vol

# Problematique des modèles d'intelligence artificielles
```
ChatGPT === wikipedia + reddit + twitter + marmiton + ...
```

---

Le problème des algorithmes de recommandation:
- cambridge analytica
- Qanon
- etc...

---

des outils:
- Opaques
- Non démocratiques
- Sans contrôle
- Construit sur une utilisation floue des données.

# Faire du cash

- Il est possible de monter une entreprise open source, et possède meme plusieurs avantages:
  - Soutiens psychologique de la communauté.
  - Aides de la région
  - Oblige à concevoir de meilleurs produits
  - Facilités de recrutement

# Bibliographie

- [https://fr.wiktionary.org/wiki/soustraction](https://fr.wiktionary.org/wiki/soustraction)
- [https://opentf.org/](https://opentf.org/)
- [https://fr.wikipedia.org/wiki/Bien_immatériel](https://fr.wikipedia.org/wiki/Bien_immat%C3%A9riel)
- **Le Code de la Propriété Intellectuelle:**
    - **[article L111-1](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006278868&cidTexte=LEGITEXT000006069414)**
    - **[article 113-9](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006278890&cidTexte=LEGITEXT000006069414)**
- Code pénal:
    - **[Article 311-1](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000006418127)**
    - [https://www.legifrance.gouv.fr/codes/section_lc/LEGITEXT000006070719/LEGISCTA000006165324/](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000006418127)
- **[Capital social (sciences sociales) — Wikipédia](https://fr.wikipedia.org/wiki/Capital_social_(sciences_sociales)#:~:text=social%20et%20%C3%A9conomique.-,Chez%20Bourdieu,capital%20symbolique%20et%20capital%20social.)** (Accessed on 09/05/2023)
- *Les formes de capital* (1986) - Bourdieu
- **[The cloud is just another sun - YouTub - Maddog](https://www.youtube.com/watch?v=goQ-yjN7WAc)** (Accessed on 09/05/2023)
- Blast science 4 all:
    - [https://www.youtube.com/watch?v=kR3JaROyKuA&t=1330s](https://www.youtube.com/watch?v=kR3JaROyKuA&t=1330s)
- Note du conseil d’état: le code appartient aux stagiaires
    - [https://www.legifrance.gouv.fr/ceta/id/CETATEXT000021880330/](https://www.legifrance.gouv.fr/ceta/id/CETATEXT000021880330/)
