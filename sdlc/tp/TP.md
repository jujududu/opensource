# TP2 Jeudi: 
# TP SDLC
```
──(kali㉿kali)-[~/…/opensource/opensource/sdlc/tp]
└─$ git clone https://github.com/projectdiscovery/nuclei.git       
Cloning into 'nuclei'...
remote: Enumerating objects: 38154, done.
remote: Counting objects: 100% (4972/4972), done.
remote: Compressing objects: 100% (2410/2410), done.
remote: Total 38154 (delta 2999), reused 3883 (delta 2185), pack-reused 33182
Receiving objects: 100% (38154/38154), 59.16 MiB | 1.53 MiB/s, done.
Resolving deltas: 100% (26270/26270), done.
                                                                             
┌──(kali㉿kali)-[~/…/opensource/opensource/sdlc/tp]
└─$ cd nuclei 
                                                                             
┌──(kali㉿kali)-[~/…/opensource/sdlc/tp/nuclei]
└─$ git checkout d051332d                                          
Note: switching to 'd051332d'.

You are in 'detached HEAD' state. You can look around, make experimental
changes and commit them, and you can discard any commits you make in this
state without impacting any branches by switching back to a branch.

If you want to create a new branch to retain commits you create, you may
do so (now or later) by using -c with the switch command. Example:

  git switch -c <new-branch-name>

Or undo this operation with:

  git switch -

Turn off this advice by setting config variable advice.detachedHead to false

HEAD is now at d051332d chore(deps): bump golang.org/x/net from 0.14.0 to 0.17.0 in /v2 (#4233)
                                                                             
                                                                             
┌──(kali㉿kali)-[~/…/opensource/sdlc/tp/nuclei]
└─$ git apply -v 0001-fix-patch.patch 
Checking patch v2/internal/installer/versioncheck.go...
Checking patch v2/internal/runner/runner_test.go...
Checking patch v2/pkg/model/worflow_loader.go => v2/pkg/model/workflow_loader.go...
Checking patch v2/pkg/protocols/headless/engine/page_actions_test.go...
Applied patch v2/internal/installer/versioncheck.go cleanly.
Applied patch v2/internal/runner/runner_test.go cleanly.
Applied patch v2/pkg/model/worflow_loader.go => v2/pkg/model/workflow_loader.go cleanly.
Applied patch v2/pkg/protocols/headless/engine/page_actions_test.go cleanly.

```
## Environment de dev
```
┌──(kali㉿kali)-[~/…/opensource/sdlc/tp/nuclei]
└─$ cat flake.nix 
{
  description = "Nuclei";
  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    utils.url = "github:numtide/flake-utils";
  };

  outputs =
    { self
    , nixpkgs
    , utils
    ,
    }:
    utils.lib.eachDefaultSystem (system:
    let
      pkgs = import nixpkgs {
        inherit system;
        config = {
          allowBroken = true;
          allowUnsupportedSystem = true;
        };
      };
    in
    {

      devShells.default = pkgs.mkShell {
        buildInputs = with pkgs;
          [
            go
            gopls
            gotools
            go-tools
          ];
      };
    });
}
                                                        
┌──(kali㉿kali)-[~/…/opensource/sdlc/tp/nuclei]
└─$ git add .        
                                               

┌──(kali㉿kali)-[~/…/opensource/sdlc/tp/nuclei]
└─$ cat .envrc   
# SPDX-License-Identifier: AGPL-3.0-or-later
if ! has nix_direnv_version || ! nix_direnv_version 2.3.0; then
  source_url https://raw.githubusercontent.com/nix-community/nix-direnv/2.3.0/direnvrc sha256-Dmd+j63L84wuzgyjITIfSxSD57Tx7v51DMxVZOsiUD8=
fi
nix_direnv_watch_file flake.nix
nix_direnv_watch_file flake.lock
use flake

┌──(kali㉿kali)-[~/Documents/OpenSource/opensource/opensource]
└─$ cat .gitignore 
**/.vagrant
.task
.direnv
result
.direnv

            
```                                     

question :
Une implémentation plus rapide et persistante de direnv's use_nixet use_flake, pour remplacer celle intégrée.

Caractéristiques principales:

beaucoup plus rapide après la première exécution grâce à la mise en cache de l' nix-shellenvironnement
empêche le garbage collection des dépendances de build en créant un lien symbolique entre la dérivation shell résultante dans celle de l'utilisateur gcroots(la vie est trop courte pour perdre le cache de build de votre projet si vous êtes sur un vol sans connexion Internet)

question :
Renseignez vous sur le mot clef deps et ses propriétés dans la documentation, quel est sont plus grand avantage ?


## Build system
```
┌──(kali㉿kali)-[~/…/opensource/sdlc/tp/nuclei]
└─$ touch Taskfile.yaml
                                                                                                                    
┌──(kali㉿kali)-[~/…/opensource/sdlc/tp/nuclei]
└─$ ls -a 
.        .github               DEBUG.md    README_CN.md         Taskfile.yaml  integration_tests
..       .gitignore            DESIGN.md   README_ID.md         docs           nuclei-jsonschema.json
.direnv  .run                  Dockerfile  README_KR.md         flake.lock     static
.envrc   0001-fix-patch.patch  LICENSE.md  SYNTAX-REFERENCE.md  flake.nix      tp.direnvrc
.git     CONTRIBUTING.md       README.md   THANKS.md            helm           v2
                                                                                                                    
┌──(kali㉿kali)-[~/…/opensource/sdlc/tp/nuclei]
└─$ nano Taskfile.yaml 
                                                                                                                    
┌──(kali㉿kali)-[~/…/opensource/sdlc/tp/nuclei]
└─$ nano Taskfile.yaml 
                                                                                                                    
┌──(kali㉿kali)-[~/…/opensource/sdlc/tp/nuclei]
└─$ task               
task: [init:go] go mod download
                                                                                                                    
task init build                                                                                                     


task: Available tasks for this project:
* build:         Build nuclei
* default:       Display help
* init:          Setup project
                                                                                                                    
┌──(kali㉿kali)-[~/…/opensource/sdlc/tp/nuclei]
└─$ task init build
task: Task "init:go" is up to date
task: Task "init:go" is up to date                                                                                  
internal/unsafeheader   
...
...

                                                                                                              
┌──(kali㉿kali)-[~/…/opensource/sdlc/tp/nuclei]
└─$ ./v2/nuclei

                     __     _
   ____  __  _______/ /__  (_)
  / __ \/ / / / ___/ / _ \/ /
 / / / / /_/ / /__/ /  __/ /
/_/ /_/\__,_/\___/_/\___/_/   v2.9.15

                projectdiscovery.io

[INF] nuclei-templates are not installed, installing...
[INF] Successfully installed nuclei-templates at /home/kali/nuclei-templates
[INF] Current nuclei version: v2.9.15 (latest)
[INF] Current nuclei-templates version: v9.6.6 (latest)
[INF] New templates added in latest release: 162
[INF] Templates loaded for current scan: 7129
[INF] No results found. Better luck next time!
                                                   
    
```
question : 
Renseignez vous sur le mot clef deps et ses propriétés dans la documentation, quel est sont plus grand avantage ?

le mot clef deps est utile pour les "Task dependencies",
les dépendences des taches de notre taskfile. 
Quand on les précise dans le mot clef deps, 
on les lances automatiquement avent les taches parents

## Pure

question : 
Comment faire si le hash du fichier ./v2/go.mod change et donc la valeur de vendorSha256 (indice c'est dans la doc)?
on dois mettre à jour l'attribut vendorHash.

```Bash
┌──(kali㉿kali)-[~/…/opensource/sdlc/tp/nuclei]
└─$ nix build '.'                                 
warning: Git tree '/home/kali/Documents/OpenSource/opensource/opensource/sdlc/tp/nuclei' is dirty                                                                                                                                                                                                                        
┌──(kali㉿kali)-[~/…/opensource/sdlc/tp/nuclei]
└─$ ls                     
0001-fix-patch.patch  DEBUG.md   LICENSE.md  README_CN.md  README_KR.md         THANKS.md      docs        flake.nix  integration_tests       result  tp.direnvrc
CONTRIBUTING.md       DESIGN.md  README.md   README_ID.md  SYNTAX-REFERENCE.md  Taskfile.yaml  flake.lock  helm       nuclei-jsonschema.json  static  v2
             
```
```Bash

┌──(kali㉿kali)-[~/…/opensource/sdlc/tp/nuclei]
└─$ ls -la | grep result
lrwxrwxrwx  1 kali kali    50 Nov  2 09:57 result -> /nix/store/mpk8wf498zlbzsgd8w8bhwsnvahm087a-nuclei
                                                                                                                                                                                                                                           
┌──(kali㉿kali)-[~/…/opensource/sdlc/tp/nuclei]
└─$ ls -la result
lrwxrwxrwx 1 kali kali 50 Nov  2 09:57 result -> /nix/store/mpk8wf498zlbzsgd8w8bhwsnvahm087a-nuclei
                                                                                                                                                                                                                                           
┌──(kali㉿kali)-[~/…/opensource/sdlc/tp/nuclei]
└─$ result/bin/nuclei   

                     __     _
   ____  __  _______/ /__  (_)
  / __ \/ / / / ___/ / _ \/ /
 / / / / /_/ / /__/ /  __/ /
/_/ /_/\__,_/\___/_/\___/_/   v2.9.15

                projectdiscovery.io
```
## Container

que pouvez vous en dire par rapport à votre Taskfile? Essayez d'etre critique.
le taskfile est plus detailler que le dockerfile
tout en étant moins verbeux et évite les répétitions (dans le docker RUN ... FROM ...)

```Bash
                                                                                                                                                                                                                                           
warning: Git tree '/home/kali/Documents/OpenSource/opensource/opensource/sdlc/tp/nuclei' is dirty
warning: Git tree '/home/kali/Documents/OpenSource/opensource/opensource/sdlc/tp/nuclei' is dirty
┌──(kali㉿kali)-[~/…/opensource/sdlc/tp/nuclei]
└─$ rm Dockerfile 
                                                                                                                                                                                                                                        

 # on build avec nix build aprés avoir modifier le Flakenix
┌──(kali㉿kali)-[~/…/opensource/sdlc/tp/nuclei]
└─$ nano flake.nix
                                                                                                                                                                                                                                           
warning: Git tree '/home/kali/Documents/OpenSource/opensource/opensource/sdlc/tp/nuclei' is dirty
warning: Git tree '/home/kali/Documents/OpenSource/opensource/opensource/sdlc/tp/nuclei' is dirty
┌──(kali㉿kali)-[~/…/opensource/sdlc/tp/nuclei]
└─$ nix build '.#container'
warning: Git tree '/home/kali/Documents/OpenSource/opensource/opensource/sdlc/tp/nuclei' is dirty
                                                                                                                                                                                                                                           
┌──(kali㉿kali)-[~/…/opensource/sdlc/tp/nuclei]
└─$ ls -la result

lrwxrwxrwx 1 kali kali 72 Nov  2 10:14 result -> /nix/store/6sk950qmvmm9jgfqwdwi8lrkwmh14nji-docker-image-tldr-nix.tar.gz
                                                                                                                                           
```
```Bash
┌──(kali㉿kali)-[~/…/opensource/sdlc/tp/nuclei]
└─$ nix run 'nixpkgs#podman' -- load -i result                
Getting image source signatures
Copying blob 15d572f1145a done   | 
Copying config cd49e37ade done   | 
Writing manifest to image destination
Loaded image: localhost/tldr-nix:6sk950qmvmm9jgfqwdwi8lrkwmh14nji
                                                                                                                                                                                                                                           
┌──(kali㉿kali)-[~/…/opensource/sdlc/tp/nuclei]
└─$ nix run 'nixpkgs#podman' -- run -it -v /tmp:/tmp localhost/tldr-nix:6sk950qmvmm9jgfqwdwi8lrkwmh14nji nuclei
[ERR] failed to load provider keys got [uncover:RUNTIME] provider config file .config/uncover/provider-config.yaml does not exist

                     __     _
   ____  __  _______/ /__  (_)
  / __ \/ / / / ___/ / _ \/ /
 / / / / /_/ / /__/ /  __/ /
/_/ /_/\__,_/\___/_/\___/_/   v2.9.15

                projectdiscovery.io



```
## FMT
aprés l'ajout des packages :
- nixpkgs-fmt
réecrit le code dans un format standart
[nix info](https://nixos.org/manual/nix/stable/command-ref/new-cli/nix3-fmt)
- alejandra
c'est un package qui fais du formatage
[GitHub](https://github.com/kamadorueda/alejandra) qui parle de alejandra
- statix
met en évidence les redondences dans le code Nix
[GitHub](https://github.com/nerdypepper/statix/tree/master) qui en parle

```Bash
nix fmt        
warning: Git tree '/home/kali/Documents/OpenSource/opensource/opensource/sdlc/tp/nuclei' is dirty
+ alejandra .
Checking style in 1 file using 4 threads.

Formatted: ./flake.nix

Success! 1 file was formatted.

👏 Special thanks to GuangTao Zhang for being a sponsor of Alejandra!
+ nixpkgs-fmt .
./flake.nix
1 / 1 have been reformatted
+ statix fix '*.nix'
config error: path error: file not found: *.nix
```




──(kali㉿kali)-[~/…/opensource/sdlc/tp/nuclei]
└─$ nix flake check
warning: Git tree '/home/kali/Documents/OpenSource/opensource/opensource/sdlc/tp/nuclei' is dirty
error: builder for '/nix/store/06q9x2r0z8xbsadz4pa6mbsd1kd7p4k4-nuclei.drv' failed with exit code 1;
       last 10 log lines:
       >                                --- Expected
       >                                +++ Actual
       >                                @@ -1 +1 @@
       >                                -_IWP_JSON_PREFIX_eyJpd3BfYWN0aW9uIjoiYWRkX3NpdGUiLCJwYXJhbXMiOnsidXNlcm5hbWUiOiIfX0=
       >                                +_IWP_JSON_PREFIX_eyJpd3BfYWN0aW9uIjoiYWRkX3NpdGUiLCJwYXJhbXMiOnsidXNlcm5hbWUiOiIifX0=
       >                Test:           TestEvaluate
       >                Messages:       could not get correct expression
       > FAIL
       > FAIL   github.com/projectdiscovery/nuclei/v2/pkg/protocols/common/expressions  0.198s
       > FAIL
       For full logs, run 'nix log /nix/store/06q9x2r0z8xbsadz4pa6mbsd1kd7p4k4-nuclei.drv'.
                                                                                                                                                                                                                                           
┌──(kali㉿kali)-[~/…/opensource/sdlc/tp/nuclei]
└─$ grep TestEvaluate -r             

v2/pkg/protocols/common/expressions/expressions_test.go:func TestEvaluate(t *testing.T) {
                                                                                                                                                                                                                                           
┌──(kali㉿kali)-[~/…/opensource/sdlc/tp/nuclei]
└─$ rm v2/pkg/protocols/common/expressions/expressions_test.go
                                                                                                                                                                                                                                           
┌──(kali㉿kali)-[~/…/opensource/sdlc/tp/nuclei]
└─$ nix flake check                                           
warning: Git tree '/home/kali/Documents/OpenSource/opensource/opensource/sdlc/tp/nuclei' is dirty
warning: The check omitted these incompatible systems: aarch64-darwin, aarch64-linux, x86_64-darwin
Use '--all-systems' to check all.
                                                                                                                                                                                                                                           
┌──(kali㉿kali)-[~/…/opensource/sdlc/tp/nuclei]
└─$ nix run 'nixpkgs#podman' -- load -i result  
Error: payload does not match any of the supported image formats:
 * oci: open /etc/containers/policy.json: no such file or directory
 * oci-archive: open /etc/containers/policy.json: no such file or directory
 * docker-archive: open /etc/containers/policy.json: no such file or directory
 * dir: open /etc/containers/policy.json: no such file or directory
                                                                                                                                                                                                                                           
┌──(kali㉿kali)-[~/…/opensource/sdlc/tp/nuclei]
└─$ mkdir -p ~/.config/containers/
cat <<EOF > ~/.config/containers/containers.conf
[engine]
events_logger = "file"
cgroup_manager = "cgroupfs"
EOF
cat <<EOF > ~/.config/containers/policy.json 
{
  "default": [
    { "type": "insecureAcceptAnything" }
  ],
  "transports": {
    "docker-daemon": {
      "": [
        { "type": "insecureAcceptAnything" }
      ]
    }
  }
}
EOF

                                                                                                                                                                                                                                           
┌──(kali㉿kali)-[~/…/opensource/sdlc/tp/nuclei]
└─$ nix run 'nixpkgs#podman' -- load -i result                
Getting image source signatures
Copying blob 15d572f1145a done   | 
Copying config cd49e37ade done   | 
Writing manifest to image destination
Loaded image: localhost/tldr-nix:6sk950qmvmm9jgfqwdwi8lrkwmh14nji
                                                                                                                                                                                                                                           
┌──(kali㉿kali)-[~/…/opensource/sdlc/tp/nuclei]
└─$ nix run 'nixpkgs#podman' -- run -it -v /tmp:/tmp localhost/tldr-nix:6sk950qmvmm9jgfqwdwi8lrkwmh14nji nuclei
[ERR] failed to load provider keys got [uncover:RUNTIME] provider config file .config/uncover/provider-config.yaml does not exist

                     __     _
   ____  __  _______/ /__  (_)
  / __ \/ / / / ___/ / _ \/ /
 / / / / /_/ / /__/ /  __/ /
/_/ /_/\__,_/\___/_/\___/_/   v2.9.15

                projectdiscovery.io

[INF] nuclei-templates are not installed, installing...
[ERR] Could not read nuclei-ignore file: open .config/nuclei/.nuclei-ignore: no such file or directory
[ERR] Could not find template '/nuclei-templates': no templates found in path /nuclei-templates
[INF] Current nuclei version: v2.9.15 (outdated)
[INF] Current nuclei-templates version:  (latest)
[INF] No results found. Better luck next time!
[FTL] Could not run nuclei: no valid templates were found
                                                                                                                                                                                                                                           
┌──(kali㉿kali)-[~/…/opensource/sdlc/tp/nuclei]
└─$ 
### Taskfile x nix les bros

on modifie le Taskfile puis on fais les différentes étapes demander :
```Bash
┌──(kali㉿kali)-[~/…/opensource/sdlc/tp/nuclei]
└─$ task build        
task: Task "init:go" is up to date
warning: Git tree '/home/kali/Documents/OpenSource/opensource/opensource/sdlc/tp/nuclei' is dirty
                                                                                                                                                                                                                                           
┌──(kali㉿kali)-[~/…/opensource/sdlc/tp/nuclei]
└─$ ls -la result 
lrwxrwxrwx 1 kali kali 50 Nov 10 08:57 result -> /nix/store/ykxglw7lrkcz7if8gff97j01pwnw4sr4-nuclei
                                                                                                                                                                                                                                           
┌──(kali㉿kali)-[~/…/opensource/sdlc/tp/nuclei]
└─$ task build:container
task: Task "init:go" is up to date
warning: Git tree '/home/kali/Documents/OpenSource/opensource/opensource/sdlc/tp/nuclei' is dirty
warning: error: unable to download 'https://cache.nixos.org/nix-cache-info': Couldn't resolve host name (6); retrying in 339 ms
warning: error: unable to download 'https://cache.nixos.org/nix-cache-info': Couldn't resolve host name (6); retrying in 590 ms
warning: error: unable to download 'https://cache.nixos.org/nix-cache-info': Couldn't resolve host name (6); retrying in 1310 ms
warning: error: unable to download 'https://cache.nixos.org/nix-cache-info': Couldn't resolve host name (6); retrying in 2804 ms
warning: unable to download 'https://cache.nixos.org/nix-cache-info': Couldn't resolve host name (6)
                                             
```
### Pre-commit hooks:

Expliquex moi ce que vous venez de faire la avec ce shellHook ?
...

