# La gestion de conf avec Ansible

## preambule

- Présentation de [la gestion des configuration](./presentation-gestion-conf.md)
- Pour ceux qui ne connaissent pas une présentation de l'outil [Vagrant](./vagrant.md) qui est bien utile pour ce cours.

## Debuter avec Ansible

Pour ceux qui ne connaissent pas Ansible :

- Pour comprendre et utiliser ansible : [Présentation Ansible](./presentation-ansible.md)
- Pour comprendre et [Ecrire du code Ansible](./code-ansible.md)

## Un peu plus d'Ansible

- Quelques [pratiques Ansible](./pratiques-ansible.md) permettant d'organiser son code ansible.
- Une petite [présentaiton de l'outil ansible molécule](./ansible-molecule.md) permettant d'automatiser les tests des roles Ansible.
